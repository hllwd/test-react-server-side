'use strict'

var gulp = require('gulp')
var concat = require('gulp-concat')

gulp.task('build-vendors', function () {
    gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/react/dist/react.js'
    ])
        .pipe(concat('vendors.min.js'))
        .pipe(gulp.dest('./public/js'))
})

// default task
gulp.task('default', ['build-vendors']);