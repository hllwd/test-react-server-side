'use strict'

// react conf
require('babel/register');
require('node-jsx').install({extension: '.jsx'})
var React = require('react')

// components
var Container = require('../web_modules/components/Container')

var getDataLive = require('../services/getLive')

exports.index = function (req, res) {
    getDataLive().then(function (data) {
        res.render('index', {
            container: React.renderToString(<Container initialData={data}/>),
            dataJson: JSON.stringify(data)
        })
    })
}