'use strict'

require('node-jsx').install({extension: '.jsx'})

var express = require('express')
var bodyParser = require('body-parser')
var methodOverride = require('method-override')
var http = require('http')
var path = require('path')
var routes = require('./routes/index.jsx')
var app = module.exports = express()

app.set('port', 3017)
app.set('views', path.resolve('views'))
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(methodOverride())
app.use(express.static(path.resolve('public')))

app.get('*', routes.index)

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'))
})