'use strict'

import React from 'react'

export default class Match extends React.Component {

    render() {

        const { nom_rencontre, logo1, score1, logo2, score2 } = this.props.match

        return (
            <li>
                <p>{nom_rencontre}</p>
                <div>
                    <img src={logo1}/>
                    {score1}
                </div>
                <div>
                    <img src={logo2}/>
                    {score2}
                </div>
            </li>
        )
    }
}