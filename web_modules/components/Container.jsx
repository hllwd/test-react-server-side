import React from 'react'
import Journee from './Journee.jsx'
import getLive from '../services/getLive.js'
import injectTapEventPlugin from 'react-tap-event-plugin'; injectTapEventPlugin()

export default
class Container extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.props.initialData,
            refresh: 0
        }
    }

    retrieveData() {
        getLive().then(data => this.setState({
            refresh: this.state.refresh + 1,
            data: data
        }))
    }

    componentDidMount() {
        this.interval = window.setInterval(this.retrieveData.bind(this), 1000 * 5)
    }


    componentWillUnmount() {
        window.clearInterval(this.interval)
    }

    render() {
        
        let { refresh, data } = this.state
        
        return (
            <div>
                <div>refresh : {refresh}</div>
                {data.map( (d, k) =>  <Journee journee={d.journee} matchs={d.matchs} key={k}/> )}
            </div>
        )
    }
}