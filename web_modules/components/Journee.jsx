'use strict'

import React from 'react'
import Match from './Match.jsx'

export default class Journee extends React.Component {

    render() {

        const {nom_tour, phase, nom_saison, nom_competition, nom_sport} = this.props.journee

        return (
            <div>
                <h2>Journee : </h2>
                <div>
                    <p>{nom_tour}</p>
                    <p>{phase}</p>
                    <p>{nom_saison}</p>
                    <p>{nom_competition}</p>
                    <p>{nom_sport}</p>
                </div>
                <div>
                    <h3>matchs :</h3>
                    <ul>
                    {this.props.matchs.map((m,k)=><Match match={m} key={k}/>)}
                    </ul>
                </div>

            </div>
        )
    }
}