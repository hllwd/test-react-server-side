'use strict'

import React from 'react'
import Container from 'components/Container.jsx'

React.render(
    <Container initialData={JSON.parse(document.getElementById('data-live').innerHTML) }/>,
    document.getElementById('app-container')
)