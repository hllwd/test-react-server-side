'use strict'

import $ from 'jquery/dist/jquery.js'

export default ()=> new Promise(res => $.ajax({
    url: 'http://sport24.lefigaro.fr/livescore/rencontres-lives-home.json',
    dataType: 'json'
}).done(res))