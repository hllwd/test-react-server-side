'use strict'

var Promise = require('es6-promise').Promise
var request = require('request')

module.exports = function(){
    return new Promise(function(res){
        request.get('http://sport24.lefigaro.fr/livescore/rencontres-lives-home.json', function(error, response, body){
            if (!error && response.statusCode === 200) {
                res(JSON.parse(body))
            }
        })
    })
}